# Course
#
# Course#initialize should take the course name, department,
# and number of credits.
#
# Course#students should return a list of the enrolled students.
#
# Course#add_student should add a student to the course.
#  Probably can rely upon Student#enroll.

class Course
  attr_reader :name, :department, :credits, :students, :days, :time_block

  def initialize(name, department, credits, days = nil, time_block = nil)
    @name = name
    @department = department
    @credits = credits
    @students = []
    @days = days
    @time_block = time_block
  end

  def students
    @students
  end

  def add_student(student)
    student.enroll(self)
  end

  def conflicts_with?(course)
    # return true if course.days.include?(@days) && course.time_block == @time_block
    same_day = false
    course.days.each { |day| same_day = true if @days.include?(day) }
    return true if same_day && course.time_block == @time_block
    false
  end

end
