# Student#initialize should take a first and last name.

# Student#name should return the concatenation of the student's
# first and last name.

# Student#courses should return a list of the Courses in which
# the student is enrolled.

# Student#enroll should take a Course object, add it to the student's
# list of courses, and update the Course's list of enrolled students.
# enroll should ignore attempts to re-enroll a student.

# Student#course_load should return a hash of departments to # of
# credits the student is taking in that department.

# bundle exec rspec spec/student_spec.rb
# bundle exec rspec spec/course_spec.rb
# bundle exec rspec spec/extension_spec.rb

class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
    # @enroll = []
    # @course_load = {}
    # specs tell if need instance variable by calling
    # methods on them
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  # Student#courses should return a list of the Courses in which
  # the student is enrolled.
  def courses
    @courses
  end

  # Student#enroll should take a Course object, add it to the student's
  # list of courses, and update the Course's list of enrolled students.
  def enroll(course)

    raise_error "conflict" if has_conflict?(course)
    @courses << course unless @courses.include?(course)

    # Course.add_student
    course.students << self


  end

  def has_conflict?(new_course)
    #REMEMBER YOUR OTHER ENUMERABLES
    @courses.any? do |enrolled|
      new_course.conflicts_with?(enrolled)
    end
  end

  # Student#course_load should return a hash of departments to # of
  # credits the student is taking in that department.
  def course_load
    load = Hash.new(0)
    courses.map { |course| load[course.department] += course.credits }
    load
  end
end
